CC = gcc
CFLAGS = -Wall -Wextra -O0 -g
BUILD_DIR = build

SRCS = $(wildcard src/*.c) $(wildcard src/*/*.c)
OBJS = $(patsubst src/%c,$(BUILD_DIR)/%o,$(SRCS))
EXEC = $(BUILD_DIR)/main

all: build_dir $(EXEC)

$(EXEC): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(EXEC)

$(BUILD_DIR)/%.o: src/%.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

build_dir:
	mkdir -p $(BUILD_DIR)

clean:
	rm -rf $(BUILD_DIR)

.PHONY: all clean

