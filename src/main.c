#include "lib/log.h"
#include "lib/persist.h"
#include "lib/row.h"
#include "lib/table.h"

#include <stdlib.h>
#include <string.h>

int main() {
  log_info("welcome to wtfdb");

  // setup types
  // TODO: refactor types out from main
  DataType DT_INT;
  DT_INT.type = INT;
  DT_INT.size = sizeof(int *);
  DT_INT.name = "integer";

  DataType DT_STR;
  DT_STR.type = STR;
  DT_STR.size = sizeof(char *);
  DT_STR.name = "string";

  // load any existing data
  init_data_dir();

  // user table setup
  TableSchema *user_tbl_schema = tbl_schema_create("user_schema");
  if (user_tbl_schema == NULL) {
    log_error("failed to create table schema");
    exit(EXIT_FAILURE);
  }

  if (tbl_schema_add_column(user_tbl_schema, "id", DT_INT.type) != 0) {
    log_error("failed to add column to table schema");
    exit(EXIT_FAILURE);
  }

  if (tbl_schema_add_column(user_tbl_schema, "name", DT_STR.type) != 0) {
    log_error("failed to add column to table schema");
    exit(EXIT_FAILURE);
  }

  if (tbl_schema_add_column(user_tbl_schema, "email", DT_STR.type) != 0) {
    log_error("failed to add column to table schema");
    exit(EXIT_FAILURE);
  }

  if (tbl_schema_add_column(user_tbl_schema, "age", DT_INT.type) != 0) {
    log_error("failed to add column to table schema");
    exit(EXIT_FAILURE);
  }

  // create a table
  Table *users_tbl = tbl_create("users", user_tbl_schema, 10);

  // create a row
  Row *row = row_create(user_tbl_schema->num_columns);

  int *id = malloc(sizeof(int));
  *id     = 1;
  row_set_value(row, 0, id);

  char *name = malloc(sizeof(char) * 10);
  strcpy(name, "John Doe");
  row_set_value(row, 1, name);

  char *email = malloc(sizeof(char) * 20);
  strcpy(email, "john@doe.com");
  row_set_value(row, 2, email);

  int *age = malloc(sizeof(int));
  *age     = 30;
  row_set_value(row, 3, age);

  // insert row into table
  log_debug("ROW A: %d\t%s\t%s\t%d", *(int *)row->values[0], (char *)row->values[1], (char *)row->values[2],
            *(int *)row->values[3]);
  tbl_insert_row(users_tbl, row);

  // insert another row

  Row *brow = row_create(user_tbl_schema->num_columns);
  int *bid  = malloc(sizeof(int));
  *bid      = 2;
  row_set_value(brow, 0, bid);

  char *bname = malloc(sizeof(char) * 10);
  strcpy(bname, "Jane Smith");
  row_set_value(brow, 1, bname);

  char *bemail = malloc(sizeof(char) * 20);
  strcpy(bemail, "jane@smith.com");
  row_set_value(brow, 2, bemail);

  int *bage = malloc(sizeof(int));
  *bage     = 25;
  row_set_value(brow, 3, bage);

  // insert row into table
  log_debug("ROW B: %d\t%s\t%s\t%d", *(int *)brow->values[0], (char *)brow->values[1], (char *)brow->values[2],
            *(int *)brow->values[3]);
  tbl_insert_row(users_tbl, brow);

  // describe
  tbl_schema_describe(user_tbl_schema);
  tbl_describe(users_tbl);

  // persist data
  if (tbl_save(*users_tbl) != 0) {
    log_error("failed to save table [%s]", users_tbl->name);
  }

  // free things
  tbl_free(users_tbl);

  log_info("bye bye");
  return 0;
}
