#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "structs.h"

StrBuf *sb_create(size_t capacity) {
  StrBuf *buf = malloc(sizeof(StrBuf));
  if (buf == NULL) {
    log_error("failed to allocate memory for StrBuf");
    return NULL;
  }

  buf->data = malloc(capacity);
  if (buf->data == NULL) {
    log_error("failed to allocate memory for StrBuf data");
    return NULL;
  }

  buf->capacity = capacity;
  buf->len      = 0;

  return buf;
}

void sb_free(StrBuf *buf) {
  free(buf->data);
  free(buf);
}

int sb_append(StrBuf *buf, const char *format, ...) {
  va_list args;
  va_start(args, format);

  // Use vasprintf to format the string
  char *formatted_str;
  int   length = vasprintf(&formatted_str, format, args);
  if (length < 0) {
    log_error("Error while formatting string");
    va_end(args);
    return -1;
  }

  // Check if the buffer has enough capacity
  // TODO: Make sb grow as needed?
  if (buf->len + length >= buf->capacity) {
    log_assert(buf->len + length < buf->capacity, "StrBuf capacity exceeded");
    free(formatted_str);
    va_end(args);
    return -1;
  }

  strcpy(buf->data + buf->len, formatted_str);
  buf->len += length;

  // cleanup
  free(formatted_str);
  va_end(args);

  return 0;
}

void sb_test() {
  StrBuf *buf = sb_create(256);
  if (buf == NULL) {
    log_error("failed to create StrBuf");
    return;
  }

  if (sb_append(buf, "hello") != 0) {
    log_error("failed to append to StrBuf");
    return;
  }

  if (sb_append(buf, " world") != 0) {
    log_error("failed to append to StrBuf");
    return;
  }

  log_info("StrBuf:\n%s", buf->data);

  sb_free(buf);
}
