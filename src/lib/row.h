#ifndef ROW_H
#define ROW_H

#include "structs.h"

Row *row_create(size_t num_values);
void row_free(Row *row);
void row_describe(Row *row, TableSchema *schema);
int  row_to_string(Row *row, TableSchema schema, StrBuf *sb);

int row_set_value(Row *row, size_t index, void *value);

#endif
