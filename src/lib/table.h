#ifndef TABLE_H
#define TABLE_H

#include <stdlib.h>

#include "structs.h"

TableSchema *tbl_schema_create(const char *name);
int          tbl_schema_add_column(TableSchema *tbl_schema, const char *name, DataTypeEnum type);
void         tbl_schema_free(TableSchema *tbl_schema);
void         tbl_schema_describe(TableSchema *tbl_schema);
int          tbl_schema_to_string(TableSchema tbl_schema, StrBuf *sb);

Table *tbl_create(const char *name, TableSchema *tbl_schema, size_t capacity);
void   tbl_free(Table *tbl);
void   tbl_describe(Table *tbl);

int tbl_insert_row(Table *tbl, Row *row);

#endif
