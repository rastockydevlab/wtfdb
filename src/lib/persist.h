#ifndef PERSIST_H
#define PERSIST_H

#include "table.h"

int init_data_dir();
int tbl_save(Table tbl);

#endif
