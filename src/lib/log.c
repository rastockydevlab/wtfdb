#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "config.h"
#include "log.h"

char err_buf[256];

void log_format(LogLevel ll, const char *tag, const char *msg, va_list args) {
  if (ll < LOG_LEVEL) {
    return;
  }

  fprintf(stdout, "%s", tag);
  vfprintf(stdout, msg, args);
  fprintf(stdout, "\033[0m\n"); // reset color and new line
}

void log_error(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  log_format(LOG_LEVEL_ERROR, "\033[31m[ERROR] ", msg, args);

  if (errno != 0) {
    sprintf(err_buf, "↳ E%d: %s", errno, strerror(errno));
    log_format(LOG_LEVEL_ERROR, "\033[31m\t", err_buf, NULL);
  }

  va_end(args);
}

void log_info(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  log_format(LOG_LEVEL_INFO, "\033[34m[INFO] ", msg, args);
  va_end(args);
}

void log_trace(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  log_format(LOG_LEVEL_TRACE, "\033[90m[TRACE] ", msg, args);
  va_end(args);
}

void log_debug(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  log_format(LOG_LEVEL_DEBUG, "\033[36m[DEBUG] ", msg, args);
  va_end(args);
}

void log_msg(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  log_format(LOG_LEVEL_ALL, "", msg, args);
  va_end(args);
}

void log_assert(int expression, const char *msg, ...) {
  if (!expression) {
    va_list args;
    va_start(args, msg);
    log_format(LOG_LEVEL_ALL, "\033[0;36m[ASSERT] ", msg, args);
    va_end(args);
    assert(expression);
  }
}
