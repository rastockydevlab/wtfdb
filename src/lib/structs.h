#ifndef STRUCTS_H
#define STRUCTS_H

#include <stdlib.h>

typedef enum { INT, STR } DataTypeEnum;

typedef struct {
  DataTypeEnum type;
  size_t       size;
  const char  *name;
} DataType;

typedef struct {
  char        *name;
  DataTypeEnum type;
} Column;

typedef struct {
  char    *name;
  Column **columns;
  size_t   num_columns;
} TableSchema;

typedef struct {
  void **values;
  size_t num_values;
} Row;

typedef struct {
  char        *name;
  TableSchema *schema;
  Row        **rows;
  size_t       num_rows;
  size_t       capacity;
} Table;

typedef struct {
  char  *data;
  size_t len;
  size_t capacity;
} StrBuf;

StrBuf *sb_create(size_t capacity);
void    sb_free(StrBuf *strbuf);
int     sb_append(StrBuf *buf, const char *format, ...);

#endif
