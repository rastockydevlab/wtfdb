#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "config.h"
#include "log.h"
#include "row.h"
#include "structs.h"
#include "table.h"

char buf[1024];

int ensure_dir_exists(const char *path, int should_create) {
  struct stat st;

  if (stat(path, &st) == -1) {
    if (!should_create) {
      log_trace("directory at %s does not exist", path);
      return 0;
    }

    if (mkdir(path, 0700) == -1) {
      log_error("failed to create directory at %s", path);
      return -1;
    } else {
      log_trace("created directory at %s", path);
    }
  } else if (!S_ISDIR(st.st_mode)) {
    log_error("[%s] is not a directory", DATA_DIR);
    return -1;
  }

  return 0;
}

int init_data_dir() {
  // check DATA_DIR exists and is a directory
  if (ensure_dir_exists(DATA_DIR, 0) != 0) {
    return -1;
  }

  // check full path of app directory, create if it doesn't exist
  char app_dir[strlen(DATA_DIR) + strlen(APP_NAME) + 2];
  sprintf(app_dir, "%s/%s", DATA_DIR, APP_NAME);

  log_debug("initializing app directory at %s", app_dir);
  if (ensure_dir_exists(app_dir, 1) != 0) {
    return -1;
  }

  return 0;
}

int tbl_save(Table tbl) {
  char tbl_dir[strlen(DATA_DIR) + strlen(APP_NAME) + strlen(tbl.name) + 3]; // 3 for "//\0"
  sprintf(tbl_dir, "%s/%s/%s", DATA_DIR, APP_NAME, tbl.name);

  // check directory for table
  log_debug("saving table at %s", tbl_dir);
  if (ensure_dir_exists(tbl_dir, 1) != 0) {
    log_error("failed to save table [%s]", tbl.name);
    return -1;
  }

  // File for schema
  char *schema_file = malloc(strlen(tbl_dir) + strlen(tbl.schema->name) + 5 + 1); // 5 for "/.txt", 1 for "\0"
  sprintf(schema_file, "%s/%s.txt", tbl_dir, tbl.schema->name);
  log_debug("writing schema file for table [%s] at [%s]", tbl.name, schema_file);

  FILE *schema_fp = fopen(schema_file, "w");
  if (schema_fp == NULL) {
    log_error("failed to open schema file for writing");
    return -1;
  }

  // create schema string
  StrBuf *buf = sb_create(256);
  if (buf == NULL) {
    log_error("failed to create StrBuf");
    return -1;
  }

  if (tbl_schema_to_string(*tbl.schema, buf) != 0) {
    log_error("failed to convert [%s] schema to string", tbl.schema->name);
    return -1;
  }

  // write schema string to file
  if (fwrite(buf->data, buf->len, 1, schema_fp) != 1) {
    log_error("failed writing to fp");
    return -1;
  }

  // reset buf
  buf->len = 0;

  // data file - csv
  char *rows_file = malloc(strlen(tbl_dir) + strlen(tbl.name) + 2 + 4);
  sprintf(rows_file, "%s/%s.csv", tbl_dir, tbl.name);
  log_debug("writing data file for table [%s] at [%s]", tbl.name, rows_file);

  FILE *rows_fp = fopen(rows_file, "w");
  if (rows_fp == NULL) {
    log_error("failed to open data file for writing");
    return -1;
  }

  // header
  for (size_t i = 0; i < tbl.schema->num_columns; i++) {
    Column col = *tbl.schema->columns[i];
    if (i < tbl.schema->num_columns - 1) {
      sb_append(buf, "%s,", col.name);
    } else {
      sb_append(buf, "%s\n", col.name);
    }
  }

  // rows
  for (size_t i = 0; i < tbl.num_rows; i++) {
    Row *row = tbl.rows[i];
    if (row == NULL) {
      log_error("row is NULL");
      return -1;
    }

    if (row_to_string(row, *tbl.schema, buf) != 0) {
      log_error("failed to convert row to string");
      return -1;
    }
  }

  // write file
  if (fwrite(buf->data, buf->len, 1, rows_fp) != 1) {
    log_error("failed writing to fp");
    return -1;
  }

  // clean up
  sb_free(buf);
  fclose(schema_fp);
  fclose(rows_fp);

  free(schema_file);
  free(rows_file);

  return 0;
}
