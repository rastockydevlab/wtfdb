#include <stdio.h>
#include <string.h>

#include "log.h"
#include "row.h"
#include "table.h"

// TableSchema

TableSchema *tbl_schema_create(const char *name) {
  log_trace("create table schema [%s]", name);

  TableSchema *tbl_schema = malloc(sizeof(TableSchema));
  if (tbl_schema == NULL) {
    log_error("failed to allocate memory for table schema");
    return NULL;
  }

  tbl_schema->name = strdup(name);
  if (tbl_schema->name == NULL) {
    log_error("failed to allocate memory for table schema name");
    return NULL;
  }

  return tbl_schema;
}

int tbl_schema_add_column(TableSchema *tbl_schema, const char *name, DataTypeEnum type) {
  log_trace("add column [%s] with type [%d] to table schema [%s]", name, type, tbl_schema->name);

  Column *col = malloc(sizeof(Column));
  if (col == NULL) {
    log_error("failed to allocate memory for column");
    return -1;
  }

  col->name = strdup(name);
  if (col->name == NULL) {
    log_error("failed to allocate memory for column name");
    return -1;
  }

  col->type = type;

  tbl_schema->columns = realloc(tbl_schema->columns, sizeof(Column *) * (tbl_schema->num_columns + 1));
  if (tbl_schema->columns == NULL) {
    log_error("failed to reallocate memory for columns");
    return -1;
  }

  tbl_schema->columns[tbl_schema->num_columns] = col;
  tbl_schema->num_columns++;

  return 0;
}

void tbl_schema_free(TableSchema *tbl_schema) {
  log_trace("free table schema [%s]", tbl_schema->name);

  for (size_t i = 0; i < tbl_schema->num_columns; i++) {
    free(tbl_schema->columns[i]->name);
    free(tbl_schema->columns[i]);
  }

  free(tbl_schema->columns);
  free(tbl_schema->name);
  free(tbl_schema);
}

void tbl_schema_describe(TableSchema *tbl_schema) {
  log_msg("\ntable schema [%s] has %zu columns", tbl_schema->name, tbl_schema->num_columns);

  for (size_t i = 0; i < tbl_schema->num_columns; i++) {
    log_msg("column [%s] has type [%d]", tbl_schema->columns[i]->name, tbl_schema->columns[i]->type);
  }
}

int tbl_schema_to_string(TableSchema tbl_schema, StrBuf *sb) {
  log_trace("convert table schema [%s] to string", tbl_schema.name);

  // name of the schema
  sb_append(sb, "%s\n", tbl_schema.name);

  // datatype, column name
  for (size_t i = 0; i < tbl_schema.num_columns; i++) {
    Column col = *tbl_schema.columns[i];
    sb_append(sb, "%d,%s\n", col.type, col.name);
  }

  return 0;
}

// Table

Table *tbl_create(const char *name, TableSchema *tbl_schema, size_t capacity) {
  log_trace("create table [%s], using schema [%s] with capacity [%zu]", name, tbl_schema->name, capacity);

  Table *tbl = malloc(sizeof(Table));
  if (tbl == NULL) {
    log_error("failed to allocate memory for table");
    return NULL;
  }

  tbl->name = strdup(name);
  if (tbl->name == NULL) {
    log_error("failed to allocate memory for table name");
    return NULL;
  }

  tbl->schema   = tbl_schema;
  tbl->capacity = capacity;
  tbl->num_rows = 0;

  tbl->rows = malloc(sizeof(Row *) * capacity);
  if (tbl->rows == NULL) {
    log_error("failed to allocate memory for table rows");
    return NULL;
  }

  return tbl;
}

void tbl_free(Table *tbl) {
  log_trace("free table [%s]", tbl->name);

  for (size_t i = 0; i < tbl->num_rows; i++) {
    row_free(tbl->rows[i]);
  }

  tbl_schema_free(tbl->schema);
  free(tbl->rows);
  free(tbl->name);
  free(tbl);
}

// TODO: refactor this mess
void tbl_describe(Table *tbl) {
  log_msg("\ntable [%s] has %zu rows\n", tbl->name, tbl->num_rows);

  TableSchema *schema = tbl->schema;
  for (size_t i = 0; i < schema->num_columns; i++) {
    printf("%s\t", schema->columns[i]->name);
  }

  printf("\n");

  for (size_t i = 0; i < tbl->num_rows; i++) {
    for (size_t j = 0; j < schema->num_columns; j++) {
      switch (schema->columns[j]->type) {
      case INT:
        printf("%d\t", *(int *)tbl->rows[i]->values[j]);
        break;
      case STR:
        printf("%s\t", (char *)tbl->rows[i]->values[j]);
        break;
      default:
        printf("NULL\t");
        break;
      }
    }

    printf("\n");
  }
}

// Rows

// TODO: find a solution for this - row is allocated outside of this scope
int tbl_insert_row(Table *tbl, Row *row) {
  log_trace("insert row into table [%s]", tbl->name);

  // TODO: make table grow as needed
  if (tbl->num_rows >= tbl->capacity) {
    log_error("table [%s] is full", tbl->name);
    return -1;
  }

  // TODO: copy memory around?
  tbl->rows[tbl->num_rows] = row;

  tbl->num_rows++;

  return 0;
}
