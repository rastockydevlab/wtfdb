#ifndef LOG_H
#define LOG_H

typedef enum {
  LOG_LEVEL_TRACE,
  LOG_LEVEL_DEBUG,
  LOG_LEVEL_INFO,
  LOG_LEVEL_ERROR,
  LOG_LEVEL_ALL, // for always visible logs: log_msg, log_assert
} LogLevel;

void log_msg(const char *msg, ...);
void log_trace(const char *msg, ...);
void log_info(const char *msg, ...);
void log_error(const char *msg, ...);
void log_debug(const char *msg, ...);
void log_assert(int expression, const char *msg, ...);

#endif
