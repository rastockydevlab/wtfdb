#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "row.h"

Row *row_create(size_t num_values) {
  log_trace("creating a row with %zu values", num_values);

  Row *row = malloc(sizeof(Row));
  if (row == NULL) {
    log_error("failed to allocate memory for a row");
    return NULL;
  }

  row->num_values = num_values;
  row->values     = malloc(sizeof(void *) * num_values);
  if (row->values == NULL) {
    log_error("failed to allocate memory for row values");
    return NULL;
  }

  return row;
}

void row_free(Row *row) {
  log_trace("free row with %zu values", row->num_values);

  for (size_t i = 0; i < row->num_values; i++) {
    free(row->values[i]);
  }

  free(row->values);
  free(row);
}

void row_describe(Row *row, TableSchema *tbl_schema) {
  log_msg("\nrow has %zu values", row->num_values);

  for (size_t i = 0; i < row->num_values; i++) {
    void *ptr = row->values[i];
    if (ptr == NULL) {
      log_msg("value [%zu] is NULL", i);
      continue;
    }

    // TODO: make a generic solution for using void ptrs
    switch (tbl_schema->columns[i]->type) {
    case INT:
      log_msg("value [%zu] is an integer: %d", i, *(int *)ptr);
      break;
    case STR:
      log_msg("value [%zu] is a string: %s", i, (char *)ptr);
      break;
    default:
      log_error("unknown data type");
      break;
    }
  }
}

int row_set_value(Row *row, size_t index, void *value) {
  log_trace("set value at index [%zu]", index);

  row->values[index] = value;

  return 0;
}

int row_to_string(Row *row, TableSchema schema, StrBuf *buf) {
  log_trace("converting row to string");

  for (size_t i = 0; i < row->num_values; i++) {
    void *ptr = row->values[i];

    switch (schema.columns[i]->type) {
    case INT:
      sb_append(buf, "%d", *(int *)ptr);
      break;
    case STR:
      sb_append(buf, "%s", (char *)ptr);
      break;
    default:
      log_error("unknown data type");
      return -1;
    }

    if (i < row->num_values - 1) {
      sb_append(buf, ",");
    } else {
      sb_append(buf, "\n");
    }
  }

  return 0;
}
